#!/bin/env bash

XD_VERSION=xdebug-2.2.5
CWD=`pwd`

TMPDIR=`mktemp -d`
echo $TMPDIR

cd $TMPDIR
wget http://xdebug.org/files/$XD_VERSION.tgz
tar xzf $XD_VERSION.tgz
cd $XD_VERSION
phpize
./configure
make
cp modules/xdebug.so /usr/lib64/php/modules

sed -i '/^\[PHP\]/a \
zend_extension = /usr/lib64/php/modules/xdebug.so \
xdebug.remote_enable = true \
xdebug.remote_connect_back = 1' /etc/php.ini

sed -i -e 's/^output_buffering =.*/output_buffering = Off/' /etc/php.ini

service httpd restart

rm -rf $TMPDIR

cd $CWD
