#!/bin/env bash

yum -y install postgresql-server
service postgresql initdb
chkconfig postgresql on
/vagrant/postgresql.sh
service postgresql start
sleep 5
su - postgres -c "psql -f /vagrant/ccportal_classical.sql"

yum -y install httpd openssl mod_ssl php php-ldap php-pdo php-pgsql

cp /vagrant/httpd-portal.conf /etc/httpd/conf.d/
cp /vagrant/portal_configuration.ini /var/www/html/
cp /vagrant/50-vagrant-mount.rules /etc/udev/rules.d/
chkconfig httpd on
sed -i -e 's/^SELINUX=.*/SELINUX=permissive/' /etc/selinux/config
setenforce 0
service httpd restart

yum -y install php-devel
/vagrant/xdebug-install.sh

chkconfig iptables off
chkconfig ip6tables off
service iptables stop
service ip6tables stop
