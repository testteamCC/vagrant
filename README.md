# README #

Vagrant bootstrapper for ccportal 

### What is this repository for? ###

* Creates CentOS image in VirtualBox
* Installs and configures all required applications
* Imports SQL dump of ccportal_classical

### How do I get set up? ###

* Install these prerequisites: VirtualBox, Vagrant
* Clone this repo
* Copy the portal SQL dumps to root of this repo, named as ccportal_dev_classicalschema.sql and ccportal_dev_classicaldata.sql
* Place a copy of the development portal_configuration.ini in the root of this repo
* Copy your ccportal working directory into the src/ folder
* Run: vagrant up
* Do work, testing at: http://localhost:8880/
* At end of day: vagrant halt

### What if I mess it up? ###

* Run: vagrant destroy
* Run: vagrant up
* Go back to work

### TODO ###

* Find a better solution for the src/ folder, perhaps locally editing the Vagrantfile with the path to your ccportal checkout.
