create database ccportal_classical;

CREATE ROLE cc_portal LOGIN
  ENCRYPTED PASSWORD 'md5b736b2e6366990fea00a8cc719640f28'
  SUPERUSER INHERIT CREATEDB CREATEROLE;

\connect ccportal_classical

\i /vagrant/ccportal_dev_classicalschema.sql
\i /vagrant/ccportal_dev_classicaldata.sql
