#!/usr/bin/env bash

sed -i -e "s/^host.*all.*all.*127\.0\.0\.1.*/\
host    all         all         127\.0\.0\.1\/32          md5 \\
host    all         all         0\.0\.0\.0\/0             md5/" /var/lib/pgsql/data/pg_hba.conf

sed -i "/^#listen_address.*/a \
listen_addresses=\'\*\' " /var/lib/pgsql/data/postgresql.conf
